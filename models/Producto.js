const mongoose = require('mongoose');

const ProductosSchema = mongoose.Schema({
    nombre: {
        type: String,
        required: true,
        trim: true
    },
    existencia: {
        type: Number,
        required: true,
        trim: true
    },
    precio: {
        type: Number,
        required: true,
        trim: true
    }, 
    creado: {
        type: Date,
        default: Date.now() 
    }
});

ProductosSchema.index({ nombre: 'text' });

module.exports = mongoose.model('Producto', ProductosSchema);

/*

type:Date,
validate: [ function(v) {
    return (v - new Date()) <= 60;
}, 'Cannot expire more than 60 seconds in the future.' ],
default: function() {
    // 60 seconds from now
    return new Date(new Date().valueOf() + 60);
}

///set default expiration 
expiracion: {
    type: Date,
    default: Date.now,
    timestamps: true,
    //     index: { expires: '1m' }
    // expires:3600,
  }

UsuariosSchema.index({ expireAt: 1 }, { expireAfterSeconds : 60 });
 */

 