const Usuario = require("../models/Usuario");
const Producto = require("../models/Producto");
const Cliente = require("../models/Cliente");
const Pedido = require("../models/Pedido");

const bcryptjs = require("bcryptjs");
const jwt = require("jsonwebtoken");

require("dotenv").config({ path: "variables.env" });

//token info displlay
const crearToken = (usuario, secreta, expiresIn) => {
  console.log(usuario);

  const { id, email, nombre, apellido } = usuario;

  return jwt.sign({ id,email,nombre,apellido }, secreta, { expiresIn });
};

//data test

//resolvers
const resolvers = {


  Query: {
    //usuario
    obtenerUsuario: async (_, {}, ctx) => {
      //const usuarioId = await jwt.verify(token, process.env.SECRETA);
      //return usuarioId;

      return ctx.usuario;
    },
    //array de productos
    obtenerProductos: async () => {
      try {
        const productos = await Producto.find({});
        return productos;
      } catch (error) {
        console.log(error);
      }
    },

    //producto individual
    obtenerProducto: async (_, { id }) => {
      //revisar si el producto existe o no
      try {
        const producto = await Producto.findById(id);
        if (!producto) {
          throw new Error("Producto no encontrado");
        }
        return producto;
      } catch (error) {
        console.log();
      }
    },
      //obtener clientes
      obtenerClientes: async () =>{
        try {
          const clientes = await Cliente.find({})
          return clientes;
  
        } catch (error) {
          console.log(error)
        }
      },
         //busqueda  de clientes al vendedor
      obtenerClientesVendedor: async(_,{},ctx) =>{
        try {
const clientes = await Cliente.find({vendedor: ctx.usuario.id.toString()});
          return clientes;
      }
      catch(error){
        console.log("error")
      }
  
    },
    
    obtenerCliente: async (_,  {id },ctx) =>{
     
        const cliente = await Cliente.findById(id);

        if(!cliente){

          throw new Error('Cliente no encontrado');
        }

        //Who made it
        //Quien lo hizo puede verlo

        if(cliente.vendedor.toString() !== ctx.usuario.id)
        {
          throw new  Error('No dispones de la credenciales para verlo');
        }

        return cliente;

     
    },
    obtenerPedidos: async() => {

      try {
        const pedidos = await Pedido.find({});
        return pedidos;
      } catch (error) {
        console.log("error")
      }

    },

    obtenerPedidosVendedor:async (_,{},ctx) => {
      try {
        const pedidos = await Pedido.find({vendedor: ctx.usuario.id}).populate('cliente');
        console.log(pedidos);
        
        return pedidos;
      } catch (error) {
        console.log(error);
      }
    },
    obtenerPedido: async(_,{id},ctx) =>{

      const pedido = await Pedido.findById(id);
      if(!pedido){
        throw new Error('Pedido no encontrado');
      }
      if(pedido.vendedor.toString() !== ctx.usuario.id){
        throw new Error('No tienes las credenciales');

      }
      return pedido;

    },
    mejoresClientes: async () => {
      const clientes = await Pedido.aggregate([
          { $match : { estado : "COMPLETADO" } },
          { $group : {
              _id : "$cliente", 
              total: { $sum: '$total' }
          }}, 
          {
              $lookup: {
                  from: 'clientes', 
                  localField: '_id',
                  foreignField: "_id",
                  as: "cliente"
              }
          }, 
          {
              $limit: 10
          }, 
          {
              $sort : { total : -1 }
          }
      ]);

      return clientes;
  }, 
  mejoresVendedores: async () => {
      const vendedores = await Pedido.aggregate([
          { $match : { estado : "COMPLETADO"} },
          { $group : {
              _id : "$vendedor", 
              total: {$sum: '$total'}
          }},
          {
              $lookup: {
                  from: 'usuarios', 
                  localField: '_id',
                  foreignField: '_id',
                  as: 'vendedor'
              }
          }, 
          {
              $limit: 3
          }, 
          {
              $sort: { total : -1 }
          }
      ]);

      return vendedores;
  },
  buscarProducto: async(_, { texto }) => {
      const productos = await Producto.find({ $text: { $search: texto  } }).limit(10)

      return productos;
  }

      
  },

  ////////////
  //Mutations
  Mutation: {
    nuevoUsuario: async (_, { input }) => {
      const { email, password } = input;

      //Check if user is registred
      const existeUsuario = await Usuario.findOne({ email });
      if (existeUsuario) {
        throw new Error("El usuario ya está registrado");
      }
      console.log(existeUsuario);

      //hash pass

      const salt = await bcryptjs.genSalt(12);
      input.password = await bcryptjs.hash(password, salt);

      //save in DB
      try {
        const usuario = new Usuario(input);
        console.log(usuario);
        usuario.save();
        //Guardarlo en la DB

        return usuario;
      } catch (error) {
        console.log(error);
      }
    },

    autenticarUsuario: async (_, { input }) => {
      const { email, password } = input;

      const existeUsuario = await Usuario.findOne({ email });

      //si el usuario no existe
      if (!existeUsuario) {
        throw new Error("El usuario no existe");
      }
      // revisar si el password concuerda con el hash
      const passwordCorrecto = await bcryptjs.compare(
        password,
        existeUsuario.password
      );
      if (!passwordCorrecto) {
        throw new Error("Password Incorrecto");
      }

      //crear token

      return {
        token: crearToken(existeUsuario, process.env.SECRETA, "24h"),
      };
    },

    nuevoProducto: async (_, { input }) => {
      try {
        const producto = new Producto(input);

        //save in db

        const resultado = await producto.save();

        return resultado;
      } catch (error) {}
    },
    //actualizacion de producto
    actualizarProducto: async (_, { id, input }) => {
      //revisar si el producto existe o no
      try {
        let producto = await Producto.findById(id);
        if (!producto) {
          throw new Error("Producto no encontrado");
        }
        //save en DB
        producto = await Producto.findOneAndUpdate({ _id: id }, input, {
          new: true,
        });

        return producto;
      } catch (error) {
        console.log();
      }
    },

    //delete producto
    eliminarProducto: async (_, { id }) => {
      let producto = await Producto.findById(id);

      try {
        if (!producto) {
          throw new Error("Producto no encontrado");
        }
      } catch (error) {
        console.log(error);
      }
      //delete
      await Producto.findOneAndDelete({ _id: id });

      return "Producto eliminado";
    },

    //Dar de alta cliente
    nuevoCliente: async (_, { input },ctx) => {

      const {email} = input;
      const cliente = await Cliente.findOne({ email });

      if (cliente) {
        throw new Error("Cliente ya está registrado");
      }


       
      //save in DB
      try {
        const nuevoCliente = new Cliente(input);
        nuevoCliente.vendedor = ctx.usuario.id;

        const resultado = await nuevoCliente.save();
        return resultado;
      } catch (error) {
        console.log(error)
      }
    },

    actualizarCliente: async (_,{id,input},ctx) =>{
     //existe
     let cliente = await Cliente.findById(id);

     if(!cliente){
       throw new Error ('Ese cliente no existe');
     }
//verificar si el vendedor es quien edita
     
     if(cliente.vendedor.toString() !== ctx.usuario.id){
    throw new Error ('No tienes las credenciales')
     }

     //save on db
     cliente = await Cliente.findOneAndUpdate({_id:id},input ,{new: true})
     return cliente;
    },



      eliminarCliente: async(_,{id},ctx) =>{

        let cliente = await Cliente.findById(id);

        if(!cliente){
          throw new Error ('Ese cliente no existe');
        }
   //verificar si el vendedor es quien edita
        
        if(cliente.vendedor.toString() !== ctx.usuario.id){
       throw new Error ('No tienes las credenciales')
        }

       await Cliente.findOneAndDelete({_id:id});
        return "Cliente eliminado"
      },



      nuevoPedido: async (_, {input}, ctx) => {

        const { cliente } = input
        
        // Verificar si existe o no
        let clienteExiste = await Cliente.findById(cliente);

        if(!clienteExiste) {
            throw new Error('Ese cliente no existe');
        }

        // Verificar si el cliente es del vendedor
        if(clienteExiste.vendedor.toString() !== ctx.usuario.id ) {
            throw new Error('No tienes las credenciales');
        }

        // Revisar que el stock este disponible
        for await ( const articulo of input.pedido ) {
            const { id } = articulo;

            const producto = await Producto.findById(id);

            if(articulo.cantidad > producto.existencia) {
                throw new Error(`El articulo: ${producto.nombre} excede la cantidad disponible`);
            } else {
                // Restar la cantidad a lo disponible
                producto.existencia = producto.existencia - articulo.cantidad;

                await producto.save();
            }
        }

        // Crear un nuevo pedido
        const nuevoPedido = new Pedido(input);

        // asignarle un vendedor
        nuevoPedido.vendedor = ctx.usuario.id;

    
        // Guardarlo en la base de datos
        const resultado = await nuevoPedido.save();
        return resultado;

        
    },
    actualizarPedido: async(_, {id, input}, ctx) => {

        const { cliente } = input;

        // Si el pedido existe
        const existePedido = await Pedido.findById(id);
        if(!existePedido) {
            throw new Error('El pedido no existe');
        }

        // Si el cliente existe
        const existeCliente = await Cliente.findById(cliente);
        if(!existeCliente) {
            throw new Error('El Cliente no existe');
        }

        // Si el cliente y pedido pertenece al vendedor
        if(existeCliente.vendedor.toString() !== ctx.usuario.id ) {
            throw new Error('No tienes las credenciales');
        }

        // Revisar el stock
        if( input.pedido ) {
            for await ( const articulo of input.pedido ) {
                const { id } = articulo;

                const producto = await Producto.findById(id);

                if(articulo.cantidad > producto.existencia) {
                    throw new Error(`El articulo: ${producto.nombre} excede la cantidad disponible`);
                } else {
                    // Restar la cantidad a lo disponible
                    producto.existencia = producto.existencia - articulo.cantidad;

                    await producto.save();
                }
            }
        }



        // Guardar el pedido
        const resultado = await Pedido.findOneAndUpdate({_id: id}, input, { new: true });
        return resultado;

    },
    eliminarPedido: async (_, {id}, ctx) => {
        // Verificar si el pedido existe o no
        const pedido = await Pedido.findById(id);
        if(!pedido) {
            throw new Error('El pedido no existe')
        }

        // verificar si el vendedor es quien lo borra
        if(pedido.vendedor.toString() !== ctx.usuario.id ) {
            throw new Error('No tienes las credenciales')
        }

        // eliminar de la base de datos
        await Pedido.findOneAndDelete({_id: id});
        return "Pedido Eliminado"
    }
}

};


module.exports = resolvers;
