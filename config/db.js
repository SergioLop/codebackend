const mongoose = require('mongoose');

require('dotenv').config({ path: 'variables.env'});

const conectarDB = async ()=>{

    try {

        
        await mongoose.connect(process.env.DB_MONGO,{
                useNewUrlParse:true,
                 useUnifiedTopolog:true,
                useFindAndModify:false,
                useCreateIndex: true

 
        });
        console.log("Db conection ready");
    } catch (error) {
        console.log('Error al conectar a la DB');
        console.log(error);
        process.exit(1);
    }
}


module.exports = conectarDB;